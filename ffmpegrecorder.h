#ifndef FFMPEGRECORDER_H
#define FFMPEGRECORDER_H

#include <mutex>
#include <condition_variable>
#include <queue>
#include <opencv2/opencv.hpp>
#include <pylon/PylonIncludes.h> //REMOVE
#include "shared_variables.h"
#include <utility>
extern std::queue<Pylon::CGrabResultPtr> ptrGrabResults;
extern std::queue<cv::Mat> images;
extern std::mutex conversionMtx;
extern std::condition_variable conversionCond;

// FFmpeg
extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libavutil/mathematics.h> //only required in older ffmpeg versions
#include <libavutil/pixdesc.h>
#include <libswscale/swscale.h>
#include <libavformat/avio.h>
#include <libavutil/imgutils.h> //for av_image_alloc only
#include <libavutil/opt.h>
}

class FfmpegRecorder
{
public:
    explicit FfmpegRecorder();
    void onVideoOpen(std::string fileName, double frameRate, int width, int height,
                     std::string codec, int bitRate, const std::vector<std::pair<std::string, std::string>>& ffmpegParameters);
    void onFrameGrabbed(const cv::Mat &);
    void processFrames(); //only used for std::threads, when used without Qt

private:
    void closeFile();
    void printAVErrorMessage(int returnCode);
    AVFormatContext* outputFormatContext;
    AVCodecContext* codecContext;
    AVCodec* vcodec;
    AVStream* vstream;
    SwsContext* swsContext;
    AVFrame* frame;
    int64_t frame_pts = 0;
    bool wasClosed = false;
    long long nImagesToGrab = 0;
    bool isRecording = false;
};

#endif // FFMPEGRECORDER_H
