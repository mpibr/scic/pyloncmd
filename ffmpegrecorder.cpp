#include "ffmpegrecorder.h"

FfmpegRecorder::FfmpegRecorder()
{
#ifdef DEBUG_BUILD
    av_log_set_level(AV_LOG_DEBUG);
#else
    av_log_set_level(AV_LOG_QUIET);
#endif
}

void FfmpegRecorder::onVideoOpen(std::string fileName, double frameRate, int width, int height,
                                 std::string codec, int bitRate, const std::vector<std::pair<std::string, std::string>>& parameters)
{
    const char *outfile = fileName.c_str();
    const char *avCodec = codec.c_str();


    const AVRational dst_fps = {int(frameRate), 1};

    // open output format context
    outputFormatContext = nullptr;
    int ret = avformat_alloc_output_context2(&outputFormatContext, nullptr, nullptr, outfile);
    if (ret < 0) {
        std::cerr << "fail to avformat_alloc_output_context2(" << outfile << "): ret=" << ret << std::endl;
        printAVErrorMessage(ret);
        return;
    }

    // create new video stream
    vcodec = avcodec_find_encoder_by_name(avCodec);
    if (!vcodec) {
        std::cerr << "Codec " << avCodec << " not found" << std::endl;
        return;
    }
    vstream = avformat_new_stream(outputFormatContext, vcodec);
    if (!vstream) {
        std::cerr << "fail to avformat_new_stream" << std::endl;
        return;
    }
    vstream->r_frame_rate = vstream->avg_frame_rate = dst_fps;

    // Allocate and initialize the codec context
    codecContext = avcodec_alloc_context3(vcodec);
    if (!codecContext) {
        std::cerr << "failed to allocate codec context" << std::endl;
        return;
    }

    // std::cout << vcodec->name << " supports these pixel formats:" << std::endl;
    // // List supported pixel formats
    // for (int i = 0; vcodec->pix_fmts[i] != AV_PIX_FMT_NONE; i++){
    //     const char *pix_fmt_name = av_get_pix_fmt_name(vcodec->pix_fmts[i]);
    //     std::cout << pix_fmt_name << std::endl;
    // }
    codecContext->width = width;
    codecContext->height = height;
    codecContext->time_base = vstream->time_base = av_inv_q(dst_fps);
    codecContext->pix_fmt = AV_PIX_FMT_BGR0; //packed RGB 8:8:8, 32bpp, RGBXRGBX... X=unused/undefined

    if (bitRate > 0){
        codecContext->bit_rate = bitRate;
    }

    // Set codec/ffmpeg specific parameters
    for (const auto& parameter : parameters){
        if(parameter.first == "rc_max_rate"){
            codecContext->rc_max_rate = stoi(parameter.second);
        }else if(parameter.first == "rc_min_rate"){
            codecContext->rc_min_rate = stoi(parameter.second);
        }else if(parameter.first == "rc_buffer_size"){
            codecContext->rc_buffer_size = stoi(parameter.second);
        }else{ //other parameters are set through priv_data
            ret = av_opt_set(codecContext->priv_data, parameter.first.c_str(), parameter.second.c_str(), 0);
            if (ret < 0) {
                std::cerr << "Failed setting codec parameter " << parameter.first << ": " << parameter.second << std::endl;
                printAVErrorMessage(ret);
            }
        }
    }

    // Open the codec for the stream
    ret = avcodec_open2(codecContext, vcodec, nullptr);
    if (ret < 0) {
        std::cerr << "Error opening codec." << std::endl;
        printAVErrorMessage(ret);
        return;
    }else{
#ifdef DEBUG_BUILD
        std::cout <<"RecordingWorker:  Opened video encoder with codec " << codec << std::endl;
#endif
    }

    // Use avcodec_parameters_from_context to initialize codecParameters
    avcodec_parameters_from_context(vstream->codecpar, codecContext);

//    std::cout
//        << "outfile: " << outfile << "\n"
//        << "format:  " << outputFormatContext->oformat->name << "\n"
//        << "vcodec:  " << vcodec->name << "\n"
//        << "size:    " << width << 'x' << height << "\n"
//        << "fps:     " << av_q2d(dst_fps) << "\n"
//        << "pixfmt:  " << av_get_pix_fmt_name(codecContext->pix_fmt) << "\n"
//        << std::flush;

    swsContext = sws_getCachedContext(
        nullptr, width, height, AV_PIX_FMT_BAYER_BGGR8,
        width, height, codecContext->pix_fmt, SWS_FAST_BILINEAR, nullptr, nullptr, nullptr);

    if (!swsContext) {
        std::cerr << "fail to sws_getCachedContext" << std::endl;
        return;
    }else{
#ifdef DEBUG_BUILD
        std::cout <<"RecordingWorker: Initialized sample scaler" << std::endl;
#endif
    }

    // prepare frame
    frame = av_frame_alloc();
    if (!frame) {
        std::cerr << "Error allocating destination frame." << std::endl;
        return;
    }
    frame->width = width;
    frame->height = height;
    frame->format = static_cast<int>(codecContext->pix_fmt);

    int alignment = 32; //1, 2, 4, 8, 16, 32, 64 etc
    ret = av_frame_get_buffer(frame, alignment);
    if (ret < 0) {
        std::cerr << "Error allocating frame data buffer." << std::endl;
        av_frame_free(&frame); // Clean up on error
        printAVErrorMessage(ret);
        return;
    }

    // open file
    ret = avio_open2(&outputFormatContext->pb, outfile, AVIO_FLAG_WRITE, nullptr, nullptr);
    if (ret < 0) {
        std::cerr << "fail to avio_open2: ret=" << ret << std::endl;
        printAVErrorMessage(ret);
        return;
    }

    // encoding loop
    ret = avformat_write_header(outputFormatContext, nullptr);
    if (ret < 0) {
        std::cerr << "failed to write header: ret=" << ret << std::endl;
        printAVErrorMessage(ret);
    }

    frame_pts = 0;
    nImagesWritten = 0;

    isRecording = true;
    wasClosed = false;
#ifdef DEBUG_BUILD
    std::cout << "RecordingWorker: Initialized NVENC encoder" << std::endl;
#endif
}

void FfmpegRecorder::onFrameGrabbed(const cv::Mat &image)
{
    // convert cv::Mat(OpenCV) to AVFrame(FFmpeg)

    // with format conversion:
    //const int stride[] = { static_cast<int>(image.step[0]) };
    //sws_scale(swsContext, &image.data, stride, 0, image.rows, frame->data, frame->linesize);

    // without format conversion:
    int cvLinesizes[1];
    cvLinesizes[0] = image.step1();
    const uint8_t* cvPtr[1];
    cvPtr[0] = image.data;
    av_image_fill_arrays(frame->data, frame->linesize, cvPtr[0], (AVPixelFormat)frame->format, frame->width, frame->height, 1);

    AVPacket* pkt;
    // encode video frame
    pkt = av_packet_alloc();

    pkt->data = nullptr;
    pkt->size = 0;
    int ret;
    bool got_pkt = false;

    while (!got_pkt) {
        if (!isRecording) {
            // Send a nullptr to signal the end of the stream
            ret = avcodec_send_frame(codecContext, nullptr);
        } else {
            // Increment pts for each frame
            frame->pts++;
            ret = avcodec_send_frame(codecContext, frame);
        }

        if (ret < 0) {
            std::cerr << "fail to avcodec_send_frame: ret=" << ret << std::endl;
            printAVErrorMessage(ret);
            return;
        }

        ret = avcodec_receive_packet(codecContext, pkt);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
            // No packet available or encoding finished
            continue;
        } else if (ret < 0) {
            std::cerr << "fail to avcodec_receive_packet: ret=" << ret << std::endl;
            printAVErrorMessage(ret);
            return;
        }

        // Set the pts and dts for the packet
        pkt->pts = av_rescale_q(frame->pts, codecContext->time_base, vstream->time_base);
        pkt->dts = av_rescale_q(frame->pts, codecContext->time_base, vstream->time_base);

        got_pkt = true;
    }

    ++nImagesWritten;

    if (got_pkt) {
        // rescale packet timestamp
        pkt->duration = 1;
        av_packet_rescale_ts(pkt, codecContext->time_base, vstream->time_base);
        // write packet
        av_write_frame(outputFormatContext, pkt);
    } else {
        std::cerr << "RecordingWorker: frame " << nImagesWritten << " was empty" << std::endl;
    }

    av_packet_unref(pkt);

}

void FfmpegRecorder::processFrames()
{
#ifdef DEBUG_BUILD
    std::cout << "running writer thread..." << std::endl;
#endif
    cv::Mat data;
    while(!acquisitionDone){
        while(nImagesWritten < nImagesConverted){
            std::unique_lock<std::mutex> lock(conversionMtx);

            conversionCond.wait(lock, []{return !images.empty();});
            data = images.front();
            images.pop();
            lock.unlock();
            this->onFrameGrabbed(data);
        }
    }
    this->closeFile();
}

void FfmpegRecorder::closeFile()
{
    //std::cout << "RecordingWorker: writing trailer" << std::endl;
    av_write_trailer(outputFormatContext);
    //std::cout << nImagesWritten << " frames encoded" << std::endl;

    av_frame_free(&frame);
    if(avcodec_is_open(codecContext)){
        avcodec_close(codecContext);
    }
    avio_close(outputFormatContext->pb);
    avformat_free_context(outputFormatContext);
    wasClosed = true;
}

void FfmpegRecorder::printAVErrorMessage(int returnCode)
{
    char buf[1024] = {0};
    av_strerror(returnCode, buf, 1024);
    std::cerr << buf << std::endl;
}
