#ifndef INPUTHANDLER_H
#define INPUTHANDLER_H
#include <thread>
#include <atomic>
#include <chrono>
#include <iostream>
#if defined USE_NCURSES
#include <ncurses.h>
#include "shared_variables.h"
#include <climits>
#endif
extern std::atomic<bool> pauseAcquisition;
extern std::atomic<bool> quitAcquisition;

class Inputhandler
{
public:
    Inputhandler();
    void handleInput();
};

#endif // INPUTHANDLER_H
