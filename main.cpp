#include "ffmpegrecorder.h"
#include "producer.h"
#include "converter.h"
#include "inputhandler.h"
#include <thread>
#include <opencv2/opencv.hpp>
#include <pylon/PylonIncludes.h>
#include <unistd.h>
#include <iostream>
#include <string>
#include <climits>

Pylon::CGrabResultPtr ptrGrabResult;
std::queue<cv::Mat> images;
std::queue<Pylon::CGrabResultPtr> ptrGrabResults;
std::mutex acquisitionMtx;
std::mutex conversionMtx;
std::condition_variable acquisitionCond;
std::condition_variable conversionCond;
std::atomic<bool> quitAcquisition(false);
std::atomic<bool> pauseAcquisition(false);
std::atomic<bool> acquisitionDone(false);
std::atomic<long long> nImagesGrabbed(0);
std::atomic<long long> nImagesConverted(0);
std::atomic<long long> nImagesWritten(0);
std::atomic<long long> nImagesToGrab(LLONG_MAX);

bool isInteractive = false;
int cameraIndex = 0;
int bitrate = -1;
std::string configFile = "";
std::string outputFileName = "";
std::string codec = "hevc_nvenc";
//std::string codec = "h264_nvenc";
//std::string codec =  "libx264"; //CPU encoding

void processInputArgument(const char& param, const std::string& value) {
    switch (param) {
    case 'c':
        configFile = value;
        break;
    case 'i':
        cameraIndex = std::stoi(value);
        break;
    case 'n':
        nImagesToGrab = std::stoll(value);
        break;
    case 'e':
        codec = value;
        break;
    case 'o':
        outputFileName = value;
        break;
    case 'b':
        bitrate = std::stoi(value);
        break;
    case 'p':
        pauseAcquisition = true;
        break;
    case 'q':
        isInteractive = false;
        break;
    default:
        std::cerr << std::endl;
        std::cerr << "Usage: pyloncmd -o outputFileName [-c configFile] [-i cameraIndex] [-gpu gpuIndex] [-n nImagesToGrab] [-e encoder] [-b bitrate] [-p (start paused)] [-q (non interactive)]" << std::endl;
        std::cerr << "Default encoder is " << codec << " other options include h264_nvenc (hardware) or libx264 (sofware)" << std::endl;
        std::cerr << std::endl;
        std::cerr << "Additional ffmpeg codec specific parameters can also be passed (e.g. -preset slow)" << std::endl;
        exit(1);
    }
}

int main(int argc, char *argv[]) {
    std::vector<std::pair<std::string, std::string>> ffmpegParameters;

    std::cout << "PylonCmd " << APP_VERSION_MAJOR << "." << APP_VERSION_MINOR << "." << APP_VERSION_PATCH <<
        " - command line tool to efficiently record from Basler cameras to h265" << std::endl;
    std::cout << "2024, Friedrich Kretschmer, Max Planck Institute for brain research" << std::endl;

 #ifdef DEBUG_BUILD
        argc = 11;
        argv[0] = (char*)"pyloncmd";
        //argv[1] = (char*)"-c/home/kretschmerf/build/pyloncmd/acA1300-30uc_21928606.pfs";
        argv[1] = (char*)"-c";
        argv[2] = (char*)"/home/kretschmerf/build/pyloncmd/acA1920-155uc_22370195.pfs";
        argv[3] = (char*)"-o";
        argv[4] = (char*)"./test.avi";
        argv[5] = (char*)"-e";
        argv[6] = (char*)"hevc_nvenc";
        argv[7] = (char*)"-n";
        argv[8] = (char*)"20";
        argv[9] = (char*)"-rc";
        argv[10] = (char*)"1";
        //argv[5] = (char*)"-g0";
        //argv[6] = (char*)"-n10000";
        std::cout << "DEBUG BUILD" << std::endl;
        for(int i = 0; i<argc; i++){
            std::cout << argv[i] << " ";
        }
        std::cout << std::endl;
#endif

    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];
        if (arg[0] == '-') {
            if (i + 1 < argc) { // Make sure we have a next argument
                std::string nextArg = argv[i + 1];
                if (nextArg[0] != '-') { // Next argument is a value, not a parameter
                    if (arg.length() == 2) { // Single-character parameter
                        processInputArgument(arg[1], nextArg);
                        ++i;
                    } else { // Multi-character parameter -> pass to ffmpeg
                        ffmpegParameters.push_back(std::make_pair(arg.substr(1), nextArg));
                        ++i;
                    }
                } else {
                    // Next argument is a parameter, so the current one doesn't have a value
                    if (arg.length() == 2) { // Single-character parameter without a value
                        processInputArgument(arg[1], "");
                    }
                }
            } else {
                // This is the last argument and it's a parameter
                if (arg.length() == 2) { // Single-character parameter without a value
                    processInputArgument(arg[1], "");
                }
            }
        }
    }

    if (outputFileName.empty()) {
        std::cerr << "Output file name is mandatory (-o outputFileName)" << std::endl;
        processInputArgument('h', ""); //show usage
        return 1;
    }

    Producer producer(configFile, cameraIndex);
    Converter converter;
    FfmpegRecorder recorder;
#ifndef DEBUG_BUILD //No keyboard interaction for Debug Build
    Inputhandler inputHandler;
#endif
    recorder.onVideoOpen(outputFileName, producer.frameRate, producer.width, producer.height, codec, bitrate, ffmpegParameters);

    long long nImagesToGrabLocal = nImagesToGrab.load();
    std::thread producerThread([&producer, nImagesToGrabLocal] { producer.acquire(nImagesToGrab); });
    std::thread converterThread([&converter] { converter.convert(); });
    std::thread recorderThread([&recorder] {recorder.processFrames(); });
    std::thread inputHandlerThread;

#ifndef DEBUG_BUILD //No keyboard interaction for Debug Build
    if(isInteractive){ //only start input thread in interactive mode
        inputHandlerThread = std::thread([&inputHandler] {inputHandler.handleInput();});
    }
#endif

    std::string modeString;
    if(nImagesToGrab == LLONG_MAX){
        modeString = " (continuous)";
    }else{
        modeString = " of total: " + std::to_string(nImagesToGrab);
    }

    // Main loop
    while (!acquisitionDone) {
        // Load atomic variables locally
        int nImagesGrabbedLocal = nImagesGrabbed.load();
        int nImagesConvertedLocal = nImagesConverted.load();
        int nImagesWrittenLocal = nImagesWritten.load();

        // Check if acquisition is done
        if ((nImagesGrabbedLocal == nImagesToGrab) && (nImagesConvertedLocal == nImagesToGrab) && (nImagesWrittenLocal == nImagesToGrab)) {
            quitAcquisition = true;
        } else {
#ifndef USE_NCURSES //the output occurs in the inputhandler when ncurses is used
            std::cout << (pauseAcquisition ? "PAUSED" : "RUNNING") <<
                " Frames grabbed: " << nImagesGrabbedLocal <<
                ", converted: " << nImagesConvertedLocal <<
                ", written: " << nImagesWrittenLocal <<
                modeString <<
                ", grabbed - written: " << (nImagesGrabbedLocal - nImagesWrittenLocal) << std::endl;
#endif
        }

        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    producerThread.join();
    converterThread.join();
    recorderThread.join();
#ifndef DEBUG_BUILD
    if(isInteractive){
        inputHandlerThread.join();
    }
#endif

    int nImagesGrabbedLocal = nImagesGrabbed.load();
    int nImagesConvertedLocal = nImagesConverted.load();
    int nImagesWrittenLocal = nImagesWritten.load();
    std::cout << "DONE" << " Frames grabbed: " << nImagesGrabbedLocal << ", converted: " << nImagesConvertedLocal << ", written: " << nImagesWrittenLocal << modeString << std::endl;
    return 0;
}
