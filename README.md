# pyloncmd
command line tool to efficiently record from Basler cameras to h265/h264

Usage: `pyloncmd -o outputFileName [-c configFile] [-i cameraIndex] [-g gpuIndex] [-n nImagesToGrab] [-e encoder] [-b bitrate] [-p]`

- `-o`: the output filename. Note that the extension specifies the container. E.g. use `myfilename.avi`
- `-c`: the configuration file with the extension `.pfs`, generated with Basler PylonViewer
- `-i`: the index of the camera, 0 = first camera
- `-g`: the index of the graphics card used for encoding, 0 = first card
- `-n`: images to grab, unlimited if omitted
- `-e`: the encoder to use (see below)
- `-b`: encoding bitrate
- `-p`: start in paused mode

Additional ffmpeg codec specific parameters can also be passed (e.g. -preset slow). Parameters for `hevc_nvenc` are listed [here](./hevc_nvenc.txt)


## Codec
Default encoder is hardware h265 (`hevc_nvenc`). Other options include 
`h264_nvenc` (hardware) or `libx264` (sofware).

## Pixelformat
To optimize bandwidth the camera has to be set to Pixel format `Bayer BG8`. 
Frames are internally interpolated to `Pylon::PixelType_BGRA8packed` 
and directly passed to the codec context as `AV_PIX_FMT_BGR0`. At the time of
writing this was the only pixel format combination that was compatible in order
to avoid additional conversion steps.


## Implementation
Hardware encoding requires an [Nvidia graphics card](https://developer.nvidia.com/video-encode-and-decode-gpu-support-matrix-new]) 
and relies on [NVENC](https://en.wikipedia.org/wiki/Nvidia_NVENC). 
This does not involve the GPU and hence a faster card will NOT gain you anything. 
Rather check the capabilities of the NVENC generation on the card and # of chips present on your card.

## Libraries
- ffmpeg (libav) for videoencoding
- ncurses for live control through a text-based UI
- [pylon SDK](https://www.baslerweb.com/en/software/pylon/sdk/) to interface with Basler cameras
