#ifndef CONVERTER_H
#define CONVERTER_H
#include <pylon/PylonIncludes.h>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <opencv2/opencv.hpp>
#include <pylon/PylonIncludes.h> //REMOVE
#include "shared_variables.h"

extern std::queue<Pylon::CGrabResultPtr> ptrGrabResults;
extern std::queue<cv::Mat> images;
extern std::mutex acquisitionMtx;
extern std::mutex conversionMtx;
extern std::condition_variable acquisitionCond;
extern std::condition_variable conversionCond;

class Converter
{
public:
    Converter();
    void convert();
private:
    Pylon::PylonAutoInitTerm autoInitTerm; //initializes Pylon for the life of this object
    Pylon::CImageFormatConverter formatConverter;
    Pylon::CPylonImage image;
};

#endif // CONVERTER_H
