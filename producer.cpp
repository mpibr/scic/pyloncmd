#include "producer.h"

void printCompatiblePixelFormats(Pylon::CImageFormatConverter *formatConverter, Pylon::EPixelType inputFormat){
    Pylon::EPixelType pixelFormats[] = {
        Pylon::PixelType_Mono1packed,
        Pylon::PixelType_Mono2packed,
        Pylon::PixelType_Mono4packed,
        Pylon::PixelType_Mono8,
        Pylon::PixelType_Mono10,
        Pylon::PixelType_Mono10packed,
        Pylon::PixelType_Mono10p,
        Pylon::PixelType_Mono12,
        Pylon::PixelType_Mono12packed,
        Pylon::PixelType_Mono12p,
        Pylon::PixelType_Mono16,
        Pylon::PixelType_BayerGR8,
        Pylon::PixelType_BayerRG8,
        Pylon::PixelType_BayerGB8,
        Pylon::PixelType_BayerBG8,
        Pylon::PixelType_BayerGR10,
        Pylon::PixelType_BayerRG10,
        Pylon::PixelType_BayerGB10,
        Pylon::PixelType_BayerBG10,
        Pylon::PixelType_BayerGR12,
        Pylon::PixelType_BayerRG12,
        Pylon::PixelType_BayerGB12,
        Pylon::PixelType_BayerBG12,
        Pylon::PixelType_BayerGR12Packed,
        Pylon::PixelType_BayerRG12Packed,
        Pylon::PixelType_BayerGB12Packed,
        Pylon::PixelType_BayerBG12Packed,
        Pylon::PixelType_BayerGR10p,
        Pylon::PixelType_BayerRG10p,
        Pylon::PixelType_BayerGB10p,
        Pylon::PixelType_BayerBG10p,
        Pylon::PixelType_BayerGR12p,
        Pylon::PixelType_BayerRG12p,
        Pylon::PixelType_BayerGB12p,
        Pylon::PixelType_BayerBG12p,
        Pylon::PixelType_BayerGR16,
        Pylon::PixelType_BayerRG16,
        Pylon::PixelType_BayerGB16,
        Pylon::PixelType_BayerBG16,
        Pylon::PixelType_RGB8packed,
        Pylon::PixelType_BGR8packed,
        Pylon::PixelType_RGBA8packed,
        Pylon::PixelType_BGRA8packed,
        Pylon::PixelType_RGB10packed,
        Pylon::PixelType_BGR10packed,
        Pylon::PixelType_RGB12packed,
        Pylon::PixelType_BGR12packed,
        Pylon::PixelType_RGB12V1packed,
        Pylon::PixelType_RGB16packed,
        Pylon::PixelType_RGB8planar,
        Pylon::PixelType_RGB16planar,
        Pylon::PixelType_YUV422packed,
        Pylon::PixelType_YUV422_YUYV_Packed,
        Pylon::PixelType_YCbCr422_8_YY_CbCr_Semiplanar,
        Pylon::PixelType_YCbCr420_8_YY_CbCr_Semiplanar,
        Pylon::PixelType_BGRA8packed,
        Pylon::PixelType_BGR8packed,
        Pylon::PixelType_RGB8packed,
        Pylon::PixelType_RGB16packed,
        Pylon::PixelType_RGB8planar,
        Pylon::PixelType_RGB16planar,
        Pylon::PixelType_Mono8,
        Pylon::PixelType_Mono16,
        Pylon::PixelType_YUV444planar,
        Pylon::PixelType_YUV422planar,
        Pylon::PixelType_YUV420planar,
    };
    std::cout << "Pylon CImageFormatConverter supports these output pixel formats for " << Pylon::CPixelTypeMapper::GetNameByPixelType(inputFormat) << ":" << std::endl;
    // Iterate through each pixel format and check if it can be converted
    for (Pylon::EPixelType pixelFormat : pixelFormats) {
        if (formatConverter->IsSupportedOutputFormat(pixelFormat)) {
            std::cout << Pylon::CPixelTypeMapper::GetNameByPixelType(pixelFormat) << std::endl;
        }
    }
}

using namespace Pylon;
Producer::Producer(std::string configFile, int cameraIndex){
    try{
        Pylon::CTlFactory& tlFactory = Pylon::CTlFactory::GetInstance();
        Pylon::DeviceInfoList_t devices;
        tlFactory.EnumerateDevices(devices, false);

        std::cout << "found: " << devices.size() << " camera(s) - ";
        if(devices.size()==0){
            throw RUNTIME_EXCEPTION("No compatible Basler cameras found");
        }

        if(size_t(cameraIndex) < devices.size()){
            if(tlFactory.IsDeviceAccessible(devices[size_t(cameraIndex)])){
                camera = new Pylon::CInstantCamera();
                camera->Attach(CTlFactory::GetInstance().CreateDevice(devices.at(cameraIndex)));
            }
        }else{
            throw RUNTIME_EXCEPTION("Camera index > detected cameras");
        }

        if(!camera->IsPylonDeviceAttached()){
            std::cerr << "Camera not attached." << std::endl;
            throw RUNTIME_EXCEPTION("Camera not attached.");
        }else{
            std::cout << camera->GetDeviceInfo().GetModelName() << std::endl;
        }
        camera->Open();

        if (!camera->IsOpen()) {
            std::cerr << "Unable to open camera!" << std::endl;
            throw RUNTIME_EXCEPTION("Unable to open camera!");
        }

        if(!configFile.empty()){
            std::ifstream file(configFile.c_str());
            if(file.good()){
                Pylon::CFeaturePersistence::Load(configFile.c_str(), &(camera->GetNodeMap()), true);
            }else{
                std::cerr << "Unable to load config file!" << std::endl;
                throw RUNTIME_EXCEPTION("Unable to load config file!");
            }
        }

        width = GenApi::CIntegerPtr(camera->GetNodeMap().GetNode("Width"))->GetValue();
        height = GenApi::CIntegerPtr(camera->GetNodeMap().GetNode("Height"))->GetValue();
        frameRate = uint(GenApi::CFloatPtr(camera->GetNodeMap().GetNode("AcquisitionFrameRate"))->GetValue());
        Pylon::EPixelType inputFormat = Pylon::EPixelType(GenApi::CEnumerationPtr(camera->GetNodeMap().GetNode("PixelFormat"))->GetIntValue());
    }catch (GenICam::GenericException &e)
    {
        // Error handling.
        std::cerr << "An exception occurred. Camera initialization failed!" << std::endl
                  << e.GetDescription() << std::endl;
        throw RUNTIME_EXCEPTION("Camera initialization failed!");
    }

}

int Producer::acquire(long long nImagesToGrab){
#ifdef DEBUG_BUILD
    std::cout << "running acquisition thread..." << std::endl;
#endif

    try{
        //printCompatiblePixelFormats(&formatConverter, inputFormat);

        camera->StartGrabbing(Pylon::GrabStrategy_OneByOne);
        auto start = std::chrono::high_resolution_clock::now();

        nImagesGrabbed = 0;
        while((!quitAcquisition))
        {
            if(!pauseAcquisition & (nImagesGrabbed < nImagesToGrab)){
                CGrabResultPtr ptrGrabResult; //pointer to grabbed image data
                camera->RetrieveResult(0xFFFFFFFF, ptrGrabResult, Pylon::TimeoutHandling_ThrowException);
                if (ptrGrabResult->GrabSucceeded())
                {
                    std::lock_guard<std::mutex> lock(acquisitionMtx);
                    ptrGrabResults.push(ptrGrabResult);
                    acquisitionCond.notify_one();
                }
                nImagesGrabbed++;
            }
        }
        acquisitionDone = true;
#ifdef DEBUG_BUILD
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> diff = end-start;
    double fps = nImagesToGrab / diff.count();
    std::cout << "Frames per second: " << fps << std::endl;
#endif

    }catch (GenICam::GenericException &e)
    {
        // Error handling.
        std::cerr << "An exception occurred." << std::endl
                  << e.GetDescription() << std::endl;
    }
    return 0;
}
