#ifndef PRODUCER_H
#define PRODUCER_H
#include <string>
#include <pylon/PylonIncludes.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <fstream>
#include <iostream>
#include <shared_variables.h>

extern std::mutex acquisitionMtx;
extern std::condition_variable acquisitionCond;
extern std::queue<Pylon::CGrabResultPtr> ptrGrabResults;
extern std::atomic<bool> pauseAcquisition;
extern std::atomic<bool> quitAcquisition;

using namespace Pylon;
class Producer
{
public:
    Producer(std::string configFile = "", int cameraIndex = 0);
    int acquire(long long nImagesToGrab);
    int width;
    int height;
    float frameRate;
    Pylon::CPylonImage image; //converted image data
    Pylon::PylonAutoInitTerm autoInitTerm; //initializes Pylon for the life of this object
    Pylon::CInstantCamera *camera = nullptr;
};

#endif // PRODUCER_H
