#include "converter.h"

Converter::Converter() {
    formatConverter.OutputPixelFormat = Pylon::PixelType_BGRA8packed;
    formatConverter.OutputBitAlignment = Pylon::OutputBitAlignment_MsbAligned;
#ifdef PYLON_VERSION_MAJOR
#if PYLON_VERSION_MAJOR > 6
#ifdef DEBUG_BUILD
    std::cout << "Pylon version is higher than 6" << std::endl;
#endif
    formatConverter.MaxNumThreads = 32;
#endif
#endif
    formatConverter.Initialize(Pylon::PixelType_BayerBG8);
}

void Converter::convert()
{
    nImagesConverted = 0;
#ifdef DEBUG_BUILD
    std::cout << "running conversion thread... " << std::endl;
#endif

    while(!acquisitionDone){
        while((nImagesConverted < nImagesGrabbed)){
            std::unique_lock<std::mutex> acquisitionLock(acquisitionMtx);
            acquisitionCond.wait(acquisitionLock, []{return !ptrGrabResults.empty();});
            Pylon::CGrabResultPtr ptrGrabResult = ptrGrabResults.front();
            ptrGrabResults.pop();
            acquisitionLock.unlock();
            formatConverter.Convert(image, ptrGrabResult);

            std::lock_guard<std::mutex> conversionLock(conversionMtx);
            images.push(cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3, (uint8_t*)image.GetBuffer()));
            conversionCond.notify_one();
            nImagesConverted++;
        }
    }

#ifdef DEBUG_BUILD
    std::cout << "Conversion done!" << std::endl;
#endif
}

