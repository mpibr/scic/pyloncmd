#include "inputhandler.h"

Inputhandler::Inputhandler()
{

}

void Inputhandler::handleInput()
{
#ifndef USE_NCURSES
    char ch;
    while (!quitAcquisition) {
        std::cin >> ch;
        if (std::cin.fail()) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            continue;
        }

        if (ch == 'q' || ch == 'Q') {
            quitAcquisition = true;
        } else if (ch == 'p' || ch == 'P') {
            pauseAcquisition = !pauseAcquisition;
        }
    }
#else
        char ch;
        initscr();
        cbreak();
        noecho();
        nodelay(stdscr, TRUE);// Enable non-blocking input
        //timeout(1000);
        curs_set(0);
        // Print the output at row 5, column 0
        mvprintw(1, 0, "PylonCmd %d.%d.%d", APP_VERSION_MAJOR, APP_VERSION_MINOR, APP_VERSION_PATCH);
        mvprintw(2, 0, "p = PAUSE, q = QUIT");

        int nImagesToGrabLocal = nImagesToGrab.load();

        std::string modeString;
        if(nImagesToGrab == LLONG_MAX){
            modeString = " (continuous)";
        }else{
            modeString = " of total: " + std::to_string(nImagesToGrabLocal);
        }

        while(!quitAcquisition) {
            ch = getch();
            // If a key is pressed
            if (ch != ERR) {
                if (ch == 'q' || ch == 'Q') {
                    quitAcquisition = true;
                }
                if (ch == 'p' || ch == 'P') {
                    pauseAcquisition = !pauseAcquisition;
                }
            }

            int nImagesGrabbedLocal = nImagesGrabbed.load();
            int nImagesConvertedLocal = nImagesConverted.load();
            int nImagesWrittenLocal = nImagesWritten.load();

            mvprintw(4, 0, "%s Frames grabbed: %d, converted: %d, written: %d%s",
                     pauseAcquisition ? "PAUSED" : "RUNNING",
                     nImagesGrabbedLocal,
                     nImagesConvertedLocal,
                     nImagesWrittenLocal,
                     modeString.c_str());

            refresh();          // Refresh the screen to show updates
            napms(1000);        // Sleep for 1000 milliseconds (1 second)
        }
        // End ncurses mode
        endwin();
#endif



}
