#ifndef SHARED_VARIABLES_H
#define SHARED_VARIABLES_H
#include <atomic>

extern std::atomic<bool> acquisitionDone;
extern std::atomic<long long> nImagesGrabbed;
extern std::atomic<long long> nImagesConverted;
extern std::atomic<long long> nImagesWritten;
extern std::atomic<long long> nImagesToGrab;

#endif // SHARED_VARIABLES_H
